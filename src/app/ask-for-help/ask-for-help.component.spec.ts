import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { AskForHelpComponent } from './ask-for-help.component';
import { AskForHelpService } from '../service/ask-for-help.service'
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
describe('AskForHelpComponent', () => {
  let component: AskForHelpComponent;
  let fixture: ComponentFixture<AskForHelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AskForHelpComponent ],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [BsModalRef, AskForHelpService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AskForHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

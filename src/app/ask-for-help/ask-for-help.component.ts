import { Component, OnInit } from '@angular/core';
import { AskForHelpService } from '../service/ask-for-help.service';
import { Subscription } from 'rxjs';
import { DataService } from '../service/DataService';
//import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { Router } from '@angular/router';
//import { marked } from 'angular-marked'
//import marked from 'marked';
@Component({
  selector: 'app-ask-for-help',
  templateUrl: './ask-for-help.component.html',
  styles: [
  ]
})

export class AskForHelpComponent implements OnInit {
  answer: string;
  question: string;
  subscription: Subscription;
  psubscription: Subscription;
  priority:string
//  bsModalRef: BsModalRef;

  constructor(private askforhelpService: AskForHelpService,
    //private modalService: BsModalService,
    private data: DataService, private router: Router) {
  }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(message => {
      this.question = message; if (this.question != '')
        this.save();
    });
    this.psubscription = this.data.currentPriority.subscribe(message => { this.priority = message; });

  }

 
    ticketPopup() {
      //this.save();
      //debugger;
      

      this.data.changePriority(this.priority);

      this.router.navigate(['/ticket'])
    }
 
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

   
  save() {
  //  var md = marked.setOptions({});
    
    this.askforhelpService.getAnswer(this.question).subscribe(
      (data: any) => {
        ////debugger;md.parse(this.data);
        this.answer = data.reply;
      }, err => {
        var msg="";
        if(err.status==500){
          msg="Intenal server error"
        }
        else if(err.status==404){
          msg="Data Error"
        }
        else{
          msg=err.message
        }
        const initialState = {
          errormsg: msg
        };
        //console.log(msg);
        //this.bsModalRef = this.modalService.show(ErrorModalComponent, { initialState });
      }
    );
  }
}

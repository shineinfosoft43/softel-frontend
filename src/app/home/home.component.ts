import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Home } from '../model/Home';
import { HomeService } from '../service/home.service';
import { UserDetailsService } from '../service/user-details.service';
import { Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StoreTokenService } from '../service/store-token-service.service';
import { JwtCheckService } from '../service/jwt-check.service';
import { AuthTokenService } from '../service/AuthTokenService';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userName: string;
  homeDetails: Home = new Home();
  subscription: Subscription;
  constructor(private http: HttpClient,
    private homeService: HomeService,
    private userDetailService: UserDetailsService,
    private store: StoreTokenService, private jwtcheck: JwtCheckService,
    private router: Router, private token: AuthTokenService
  ) { }

  async ngOnInit(): Promise<void> {
    this.subscription = this.token.currentToken.subscribe(message => {
      if (message != "") {
        //console.log("_______________________________________");
        //console.log(message);
        //console.log("_______________________________________");
        let auth = this.store.get("auth");
        this.jwtcheck.setToken(auth);

        //const res = await this.userDetailService.getProfile();
        ////debugger;
        const response = this.userDetailService.validateUser(this.jwtcheck.getEmailId());
        this.userName = this.jwtcheck.getUser();
        //console.log(this.userName);
       // console.log(message);
        let email = this.jwtcheck.getEmailId();
        ////debugger;
        this.homeService.getHomeDetails(email).subscribe(

          (data: Home) => {
            ////debugger
            this.homeDetails = data;
          }
        )
      }
    });

    //if(!response.isMigrationUser){
    // if(!response.isMigrationUser){

    //   this.router.navigate(['/autherror']);
    // }
    //this.router.navigate(['/askforhelp'])
    //const res = await this.userDetailService.getProfile();
    //this.userName=res.displayName

    //res.mail


  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}


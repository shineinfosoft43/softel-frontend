import { Component, OnInit } from '@angular/core';
import { PersonalMessage } from '../model/PersonalMessage';
import { PersonalMessageService } from '../service/personal-message.service';
import { UserDetailsService } from '../service/user-details.service';

@Component({
  selector: 'app-personal-message',
  templateUrl: './personal-message.component.html',
  styleUrls: ['./personal-message.component.scss']
})
export class PersonalMessageComponent implements OnInit {
  messages: PersonalMessage[];
  isTable: string = 'true';
  messageDetails: string;
  constructor(private personalMsgService: PersonalMessageService, private userDetailsService: UserDetailsService) { }

  async ngOnInit(): Promise<void> {
    const res = await this.userDetailsService.getProfile();
    //res.mail
    ////debugger;
    this.personalMsgService.getMessages(res.mail).toPromise()
      .then(data => {
        this.messages = data as PersonalMessage[];
        //console.log(JSON.stringify(this.messages));
      }

    );

   
  }

  details(message: string) {
    this.isTable = "false";
    this.messageDetails = message;
  }
  back() {
    this.isTable = "true";
  }
}


import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserLoginService } from '../service/user-login.service';

import { AuthEndComponent } from './auth-end.component';

describe('AuthEndComponent', () => {
  let component: AuthEndComponent;
  let fixture: ComponentFixture<AuthEndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthEndComponent],
      imports: [HttpClientModule],
      providers:[ UserLoginService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

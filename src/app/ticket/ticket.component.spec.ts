import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
 
import { RouterTestingModule } from '@angular/router/testing';
import {  FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { TicketComponent } from './ticket.component';
import { TicketService } from '../service/ticket.service';
import { UserDetailsService } from '../service/user-details.service';
import { DatePipe } from '@angular/common';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserLoginService } from '../service/user-login.service';

describe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TicketComponent, ModalDirective ],
      imports: [HttpClientModule,RouterTestingModule,
        FormsModule, ReactiveFormsModule, HttpClient],
      providers: [TicketService, UserDetailsService, DatePipe, UserLoginService, HttpClient]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    component.myModal = TestBed.createComponent(ModalDirective) as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import {StoreTokenService} from './service/store-token-service.service';
import { JwtCheckService } from './service/jwt-check.service';
import { UserLoginService } from './service/user-login.service';
import { log } from 'util';
@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
  constructor(private store:StoreTokenService,private jwtcheck:JwtCheckService
    ,private userlogin:UserLoginService){}
  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authtoken =this.store.get('auth')==null?"":this.store.get('auth');
    this.jwtcheck.setToken(authtoken == null ? "" : authtoken);
    ////debugger;
   if(this.jwtcheck.isTokenExpired())
      {
         this.userlogin.Login();
        authtoken =this.store.get('auth')==null?"":this.store.get('auth');
      }
    return next.handle(httpRequest.clone({ setHeaders: {
      Authorization: `Bearer ${authtoken}`
    }
  }));

  }



}


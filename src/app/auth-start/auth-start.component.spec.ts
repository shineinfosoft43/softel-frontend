import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserLoginService } from '../service/user-login.service';

import { AuthStartComponent } from './auth-start.component';

describe('AuthStartComponent', () => {
  let component: AuthStartComponent;
  let fixture: ComponentFixture<AuthStartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthStartComponent],
      //imports: [HttpClientModule],
     // providers: [UserLoginService]
      //imports: [HttpClientModule],
     // providers: [UserLoginService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

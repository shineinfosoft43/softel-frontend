import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from 'rxjs';
import { AuthTokenService } from '../service/AuthTokenService';
import { JwtCheckService } from '../service/jwt-check.service';
import { StoreTokenService } from '../service/store-token-service.service';
import { UserLoginService } from '../service/user-login.service';
import { ToastrService } from 'ngx-toastr';
import { ErrorService } from '../service/ErrorService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  subscription: Subscription;
  errorsubscription: Subscription;
  constructor(private toastr: ToastrService, private spinner: NgxSpinnerService,
    private store: StoreTokenService,
    private userservice: UserLoginService,
    private token: AuthTokenService, private router: Router,
    private errorfromapi: ErrorService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.store.remove('auth');
    this.subscription = this.token.currentToken.subscribe(message => {
      if (message != "") {
        this.router.navigate(['/home']);
        //console.log("_______________________________________");
        //console.log(message);
        //console.log("_______________________________________");
         
      }
      
    });
   
    this.errorsubscription = this.errorfromapi.currentError.subscribe(message => {
      if (message != "") {
        this.spinner.hide();
        //console.log("_______________________________________");
        //console.log(message);
        //console.log("_______________________________________");

      }

    });
    this.login();
  }
  async login() {
    this.userservice.Login();
   }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    //this.errorsubscription.unsubscribe();
  }

}

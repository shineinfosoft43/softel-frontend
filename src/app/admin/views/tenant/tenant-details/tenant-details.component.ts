import { TenantService } from './../../../common/services/tenants/tenant.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/services/common.service';
import { ToasterConfig } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tenant-details',
  templateUrl: './tenant-details.component.html',
  styleUrls: ['./tenant-details.component.scss']
})
export class TenantDetailsComponent implements OnInit {

  currentPage = 1;
  page = 1;
  addEditTenantFlag = false;
  tenantList = [];
  pageSize = 10;
  totalRecords = 0;
  search = "";
  firstEntry = 1
  lastEntry = 10
  // page = 1;

  public config1: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-right',
    animation: 'fade'
  });
  constructor(
    private router: Router,
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private commanService: CommonService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {

    // get all tenant list
    this.getTenantList();
    console.log(this.totalRecords, 'this.totalRecords');

  }

  // get Tenant list
  async getTenantList() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.fetchTenantList(this.page, this.pageSize, this.search).subscribe((res: any) => {
      if (res) {
        this.tenantList = res.results;
        this.totalRecords = res.totalNumberOfRecords;
        this.getPageEntries();
        // this.toastr.success("List")
        this.spinner.hide();
      }
      // console.log("res------>", this.tenantList);
    }, error => {
      console.log("error--->", error);
    });
  }

  //page chabge event
  pageChanged(event: any): void {
    this.page = event;
    this.getTenantList();

    // this.spinner.show();
    // this.tenantService.fetchTenantList(this.page, this.pageSize, this.search).subscribe((res: any) => {
    //   if (res) {
    //     this.tenantList = res.results;
    //     this.spinner.hide();
    //   }
    //   // console.log("res------>", this.tenantList);
    // }, error => {
    //   console.log("error--->", error);
    // });
  }

  onSearchData() {
    this.page = 1;
    this.getTenantList();
  }

  // page size change event
  async onPageSize() {
    this.page = 1;
    this.getTenantList();
    // this.spinner.show();
    // await this.tenantService.fetchTenantList(1, this.pageSize, this.search).subscribe((res: any) => {
    //   if (res) {
    //     this.tenantList = res.results;
    //     this.spinner.hide();
    //   }
    // }, error => {
    //   console.log("error--->", error);
    // });
  }

  // add or edit tenant click event
  onAddEditClick(data?) {
    // console.log("Add-edit");
    if (data?.event == 'add') {
      console.log("add--->", data);
      this.router.navigate(['admin/tenant/add-tenant', data]);
    } else {
      this.router.navigate(['admin/tenant/add-tenant', data]);
    }
  }

  // get first and last entry of table
  getPageEntries() {
    const checkFirstNumber = (this.page * this.pageSize) - this.pageSize + 1;
    if (checkFirstNumber > this.totalRecords) {
      this.firstEntry = this.totalRecords;
    } else {
      this.firstEntry = (this.page * this.pageSize) - this.pageSize + 1;
    }
    const checkLastNumber = this.page * this.pageSize;
    if (checkLastNumber > this.totalRecords) {
      this.lastEntry = this.totalRecords;
    } else {
      this.lastEntry = this.page * this.pageSize;
    }
  }

  // delete tenant detail by id
  async deleteTenant(id, tenant) {
    if (id) {
      // console.log("tenant--->", tenant)
      this.spinner.show();
      const payload: any = {
        id: id,
        createdby: tenant.tenantname,
        createddatetime: new Date(),
        isActive: false
      };
      const formData = new FormData();
      formData.append('Data', JSON.stringify(payload));
      // console.log("payload--->", payload)
      await this.tenantService.updateTenant(formData).subscribe((res: any) => {
        this.spinner.hide();
        this.toastr.success('Record Deleted Successfully');
        // this.commanService.displayToast(1, 'Deleted record successfully','success');
        this.getTenantList();
        if (res?.status === 200) {
        }
      }, (err: any) => {
        this.spinner.hide();
      });
    }
  }


  // on click to manage question to redirect on manage question page.
  onManageQuestion(id) {
    this.router.navigate(['admin/tenant/manage-questions', { ids: id }]);
  }

  // on click to redirect to manage branches page
  onManageBranches(id) {
    localStorage.setItem('tenantId', id);
    this.router.navigate(['admin/tenant/manage-batches']);
  }
}

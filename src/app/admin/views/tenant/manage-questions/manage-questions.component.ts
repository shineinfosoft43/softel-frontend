import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { TenantService } from "../../../common/services/tenants/tenant.service";
import { CommonService } from "../../../common/services/common.service";
import { NgxSpinnerService } from "ngx-spinner";
import { DndDropEvent, DropEffect } from "ngx-drag-drop";
import { ToastrService } from "ngx-toastr";


@Component({
  selector: "app-manage-questions",
  templateUrl: "./manage-questions.component.html",
  styleUrls: ["./manage-questions.component.scss"],
})
export class ManageQuestionsComponent implements OnInit {
  count = 0;
  max = 4;
  addQuestionForm: FormGroup;
  questionFormArray: FormGroup;
  questionType: any = [];
  migrationType: any = [];
  lstQuestionArray = [];
  answerArray: FormGroup;
  TanentId: any;
  tenantData = null;
  phaseData = [];
  addAnswerFlag = false;
  priorityData = [];
  questionAddedList = [];
  questionEntry = [];
  addQuestionFlag = false;
  questionFormFlag = false;
  readonlyInput = true;
  placeholderText = "";
  isEditForm = false;
  currentEditIndex = 1;
  editedSRNumber: any;

  optTypeAns = ["Opt1", "Opt2"];

  constructor(
    private fb: FormBuilder,
    private tanentService: TenantService,
    private router: Router,
    private route: ActivatedRoute,
    private _cS: CommonService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  async ngOnInit() {
    this.TanentId = this.route.snapshot.paramMap.get("ids");
    this.fbAddQuestionForm();
    this.initQuestionForm();
    this.getQuestionmessageList();
    this.getPhaseList(this.TanentId);
    this.getMigrationList(this.TanentId);
    this.getTicketPriority();
    await this.getTenantDetails();
  }

  async getTicketPriority() {
    this.spinner.show();
    await this.tanentService.getPriorityList(this.TanentId).subscribe((res: any) => {
      if (res) {
        this.priorityData = res;
        this.spinner.hide();
      }
    });
  }

  // get tenant details
  async getTenantDetails() {
    this.spinner.show();
    await this.tanentService.getTenantDetail(this.TanentId).subscribe((res: any) => {
      if (res) {
        this.tenantData = res;
        this.spinner.hide();
      }
    });
  }

  // Init Questions Form
  initQuestionForm() {
    this.questionFormArray = this.fb.group({
      question: ["", Validators.required],
      queType: ["", Validators.required],
      answer: this.fb.array([]),
      answer2: [""],
      ticket_priority: ["", Validators.required]
    });
  }

  // Configure question form
  fbAddQuestionForm() {
    this.addQuestionForm = this.fb.group({
      migrationType: ["", Validators.required],
      phaseType: ["", Validators.required]
    });
  }

  index: number = 1;

  multiselectArr() {
    return this.fb.group({
      isChecked: [false],
      TextValue: [""],
    });
  }

  get options() {
    return this.addQuestionForm.get("options") as FormArray;
  }

  get AnswerOptions() {
    return this.addQuestionForm
      .get("options")
      .get("AnswerOptions") as FormArray;
  }

  // Add New Question.
  getQuestionTypeId: any;
  addQuestion() {
    this.scrollToFormControl();
    this.questionEntry.push({ questionOrder: this.questionEntry.length + 1, data: {}, statusSave: false });
    this.questionFormArray.reset();
    this.addQuestionFlag = true;
    this.questionFormFlag = true;
  }

  addMultiSelect() {
    let formArray = this.questionFormArray.controls['answer'] as FormArray;
    formArray.push(this.fb.group(
      {
        value: [""],
      }
    ));
  }

  get formVal() {
    return <FormArray>this.addQuestionForm.get("options");
  }

  selectVal(e) {
    this.getQuestionTypeId = e.target.value;
    console.log("getQuestionTypeId--->", typeof this.getQuestionTypeId);
    if (this.getQuestionTypeId === "1") {
      this.addAnswerFlag = false;
      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      formArray.clear();
      formArray.push(this.fb.group(
        {
          value: ["Yes"],
        }
      ));
      formArray.push(this.fb.group(
        {
          value: ["No"],
        }
      ));
      this.readonlyInput = true;
    }
    else if (this.getQuestionTypeId === "2") {
      // this.questionFormArray.
      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      formArray.clear();
      formArray.push(this.fb.group(
        {
          value: [""],
        }
      ));
      formArray.push(this.fb.group(
        {
          value: [""],
        }
      ));
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = 'Option';
    }
    else if (this.getQuestionTypeId === '3') {
      // this.questionFormArray.
      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      formArray.clear();
      formArray.push(this.fb.group(
        {
          value: [''],
        }
      ));
      formArray.push(this.fb.group(
        {
          value: [''],
        }
      ));
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = 'Enter Option ';
    }
    else if (this.getQuestionTypeId === '4') {
      // this.questionFormArray.
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = '';
    }
  }

  saveQuestion() {
    // console.log("questionFormArray---->", this.questionFormArray.value);
    // console.log("addQuestionForm---->", this.addQuestionForm.value);
    if (this.isEditForm) {
      this.questionAddedList.map((val) => {
        if (val.SerialNumber === this.editedSRNumber) {
          val.JourneyID = parseInt(this.addQuestionForm.value.migrationType),
            val.Phase = this.addQuestionForm.value.phaseType,
            val.TenantID = parseInt(this.TanentId),
            val.QuestionTypeID = parseInt(this.questionFormArray.value.queType),
            val.Question = this.questionFormArray.value.question,
            val.AnswerOptions = JSON.stringify(this.questionFormArray.value.answer),
            val.TicketPriority = parseInt(this.questionFormArray.value.ticket_priority)
        }
      });
      this.isEditForm = false;
    } else {
      this.questionAddedList.push({
        "JourneyID": parseInt(this.addQuestionForm.value.migrationType),
        "Phase": this.addQuestionForm.value.phaseType,
        "TenantID": parseInt(this.TanentId),
        "SerialNumber": this.questionAddedList.length + 1,
        "QuestionTypeID": parseInt(this.questionFormArray.value.queType),
        "Question": this.questionFormArray.value.question,
        "AnswerOptions": JSON.stringify(this.questionFormArray.value.answer),
        "TicketPriority": parseInt(this.questionFormArray.value.ticket_priority),
        "CreatedBy": "p.com",
        "CreatedDatetime": new Date()
      });
      this.questionEntry[this.questionEntry.length - 1].data = this.questionAddedList[this.questionAddedList.length];
    }
    this.addQuestionFlag = false;
    this.questionFormFlag = false;
  }

  // on submit all question submit
  onAllSubmitQuestion() {
    this.spinner.show();
    this.questionAddedList.map((val) => {
      val.JourneyID = parseInt(this.addQuestionForm.value.migrationType),
        val.Phase = this.addQuestionForm.value.phaseType
    });
    this.tanentService.addQuestion(this.questionAddedList).subscribe((res: any) => {
      // console.log("res---que--->", res);
      if (res?.status === 400 || res?.status === 500) {
        // this._cS.displayToast(2, res.message);
        this.toastr.error(res.message)
        this.spinner.hide();
      } else {
        this.toastr.success('Questions Added.');
        this.spinner.hide();
        this.router.navigateByUrl('admin/tenant/tenant-details');
      }
    });
  }

  getQuestionmessageList() {
    this.tanentService.getQuestionType().subscribe((res: any) => {
      this.questionType = res;
    });
  }

  // get Phase type values for dropdown.
  getPhaseList(id) {
    this.tanentService.getPhaseType(id).subscribe((res: any) => {
      this.phaseData = res;
    });
  }

  // get Migration type values for dropdown.
  getMigrationList(id) {
    this.tanentService.getMigrationType(id).subscribe((res: any) => {
      this.migrationType = res;
    });
  }

  cancleQuestion(index: number) {
    this.options.removeAt(index);
    this.lstQuestionArray = this.addQuestionForm.get("options").value;
  }

  closeQuestion() {
    this.addQuestionFlag = false;
    this.questionFormFlag = false;
    this.getQuestionTypeId = null;
    this.questionFormArray.reset();
  }

  onBackTenant() {
    this.router.navigateByUrl('admin/tenant/tenant-details');
  }

  // get Question List
  getQuestionList() {
    if (this.addQuestionForm.controls.migrationType.value && this.addQuestionForm.controls.phaseType.value) {
      this.tanentService.getQuestionsList(this.TanentId, this.addQuestionForm.controls.migrationType.value, this.addQuestionForm.controls.phaseType.value).subscribe((res: any) => {
        if (res.length > 0) {
          res.map((val) => {
            const obj = {
              JourneyID: val.journeyID,
              Phase: val.phase,
              TenantID: parseInt(this.TanentId),
              SerialNumber: val.serialNumber,
              QuestionTypeID: val.questionTypeID,
              Question: val.question,
              AnswerOptions: val.answerOptions,
              TicketPriority: val.ticketPriority,
              CreatedBy: "p@p.com",
              CreatedDatetime: new Date(),
            }
            this.questionAddedList.push(obj);
          });
        }
      });
    }

  }

  // edit question detail by index
  editQustionList(item: any, currentIndex) {

    this.scrollToFormControl();

    this.isEditForm = true;
    this.currentEditIndex = currentIndex;
    this.editedSRNumber = item.SerialNumber

    this.getQuestionTypeId = item.QuestionTypeID;

    this.questionFormArray = this.fb.group({
      question: [item.Question, Validators.required],
      queType: [item.QuestionTypeID, Validators.required],
      answer: this.fb.array([]),
      answer2: [''],
      ticket_priority: [item.TicketPriority, Validators.required]
    });

    if (item.QuestionTypeID === 1) {
      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      const formValues = JSON.parse(item.AnswerOptions);
      formValues.map((val) => {
        formArray.push(this.fb.group({ value: [val.value] }));
      });
      this.readonlyInput = true;
      this.addAnswerFlag = false;
    } else if (item.QuestionTypeID === "2" || item.QuestionTypeID === 2) {

      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      const formValues = JSON.parse(item.AnswerOptions);
      formValues.map((val) => {
        formArray.push(this.fb.group({ value: [val.value] }));
      });
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = 'Option';
    }
    else if (item.QuestionTypeID === '3' || item.QuestionTypeID === 3) {
      let formArray = this.questionFormArray.controls['answer'] as FormArray;
      const formValues = JSON.parse(item.AnswerOptions);
      formValues.map((val) => {
        formArray.push(this.fb.group({ value: [val.value] }));
      });
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = 'Enter Option ';
    }
    else if (item.QuestionTypeID === '4') {
      this.addAnswerFlag = true;
      this.readonlyInput = false;
      this.placeholderText = '';
    }

    // this.questionEntry.push({ questionOrder: this.questionEntry.length + 1, data: item, statusSave: false });
    // this.questionFormArray.reset();
    this.addQuestionFlag = true;
    this.questionFormFlag = true;
  }

  private getTopOffset(controlEl: any): number {
    const labelOffset = 140;
    return controlEl.getBoundingClientRect().top + window.scrollY - labelOffset;
  }

  private scrollToFormControl() {
    const firstInvalidControl: any = document.getElementsByClassName("questionDiv");

    setTimeout(() => {
      window.scroll({
        top: this.getTopOffset(firstInvalidControl[0]),
        left: 0,
        behavior: "smooth",
      });
    }, 230);
  };

  onDragStart(event: DragEvent) {
  }

  onDragged(item: any, list: any[], effect: DropEffect) {

    if (effect === "move") {

      const index = list.indexOf(item);
      list.splice(index, 1);
    }
  }

  onDragEnd(event: DragEvent) {
    // this.currentDraggableEvent = event;
  }

  onDrop(event: DndDropEvent, list?: any[]) {

    if (list
      && (event.dropEffect === "copy"
        || event.dropEffect === "move")) {

      let index = event.index;

      if (typeof index === "undefined") {

        index = list.length;
      }
      list.splice(index, 0, event.data);
      list.map((val: any, index: any) => {
        val.SerialNumber = index + 1
      });
      console.log('list:', list);
    }
  }
}

import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TenantService } from './../../../../common/services/tenants/tenant.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.scss']
})
export class ViewTicketComponent implements OnInit {

  userData = null;
  ticketData = null;
  ticketList = [];
  pageSize = 10;
  page = 1;
  search = '';
  totalRecords = 0;
  firstEntry = 1;
  lastEntry = 10;

  constructor(
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.ticketData = JSON.parse(localStorage.getItem('ticketData'));
    this.getTicketList();
  }

  // get Ticket list
  async getTicketList() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.fetchTicketList(1,
      this.page, this.pageSize, this.search).subscribe((res: any) => {
        if (res) {
          this.ticketList = res.results;
          this.totalRecords = res.totalNumberOfRecords;
          this.getPageEntries();
          // this.toastr.success("List")
          this.spinner.hide();
        }
        // console.log("res------>", this.tenantList);
      }, error => {
        console.log("error--->", error);
      });
  }
  // get first and last entry of table
  getPageEntries() {
    const checkFirstNumber = (this.page * this.pageSize) - this.pageSize + 1;
    if (checkFirstNumber > this.totalRecords) {
      this.firstEntry = this.totalRecords;
    } else {
      this.firstEntry = (this.page * this.pageSize) - this.pageSize + 1;
    }
    const checkLastNumber = this.page * this.pageSize;
    if (checkLastNumber > this.totalRecords) {
      this.lastEntry = this.totalRecords;
    } else {
      this.lastEntry = this.page * this.pageSize;
    }
  }

  async onPageSize() {
    this.page = 1;
    this.getTicketList();
  }

  // on search data
  onSearchData() {
    this.page = 1;
    this.getTicketList();
  }

  //page chabge event
  pageChanged(event: any): void {
    this.page = event;
    this.getTicketList();
  }

  onView(ticket) {
    this.ticketData = ticket;
  }

  onBack() {
    this.router.navigate(['admin/tenant/manage-batches/ticket-list']);
  }

}

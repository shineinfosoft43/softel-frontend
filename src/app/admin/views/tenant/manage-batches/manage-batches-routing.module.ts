import { MessageListComponent } from './message-list/message-list.component';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageBatchesComponent } from './manage-batches.component';
import { ManageUserComponent } from './manage-user/manage-user.component';

const routes: Routes = [
  {
    path: '',
    component: ManageBatchesComponent
  },
  {
    path: 'manage-user',
    component: ManageUserComponent
  },
  {
    path: 'user-list',
    component: UserListComponent
  },
  {
    path: 'ticket-list',
    component: TicketListComponent
  },
  {
    path: 'view-ticket',
    component: ViewTicketComponent
  },
  {
    path: 'message-list',
    component: MessageListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageBatchesRoutingModule { }

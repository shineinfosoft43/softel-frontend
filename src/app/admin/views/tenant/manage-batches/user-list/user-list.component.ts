import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TenantService } from './../../../../common/services/tenants/tenant.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  batchData = null;
  tenantData = null;
  pageSize = 10;
  page = 1;
  search = "";
  userList = [];
  totalRecords = 0;
  firstEntry = 1;
  lastEntry = 10;
  prePhase: any = '';
  inPhase = '';
  postPhase = '';
  constructor(
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.tenantData = JSON.parse(localStorage.getItem('tenantData'));
    this.batchData = JSON.parse(localStorage.getItem('batch'));
    // console.log(this.tenantData)
    this.getPhaseAllocation();
    this.getBatchUsersList();
  }

  // get Phase Allocation Date
  async getPhaseAllocation() {
    this.spinner.show();
    await this.tenantService.getPhaseAllocationDate(this.tenantData.id, this.batchData.batchName).subscribe((res: any) => {
      if (res && res.length > 0) {
        // this.toastr.success("List")
        // console.log("res--->", res);
        for (let i = 0; i < res.length; i++) {
          // console.log(i)
          if (i + 1 === 1) {
            this.prePhase = res[i].phaseStartDate != null ? moment(res[i].phaseStartDate).add(1, 'days').format('yyyy-MM-DD') : '';
          } else if (i + 1 === 2) {
            this.inPhase = res[i].phaseStartDate != null ? moment(res[i].phaseStartDate).add(1, 'days').format('yyyy-MM-DD') : '';
          } else if (i + 1 === 3) {
            this.postPhase = res[i].phaseStartDate != null ? moment(res[i].phaseStartDate).add(1, 'days').format('yyyy-MM-DD') : '';
          }
          // console.log("pre--->", moment(this.prePhase).add(1,'days').format('yyyy-MM-DD'), '--in-->', this.inPhase);
        }
      }
      this.spinner.hide();
    }, error => {
      console.log("error--->", error);
    });
  }

  // get Batch Users list
  async getBatchUsersList() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.fetchBatchUsersList(this.tenantData.id, this.batchData.batchName,
      this.page, this.pageSize, this.search).subscribe((res: any) => {
        if (res) {
          this.userList = res.results;
          // this.userList = this.demoData;
          this.totalRecords = res.totalNumberOfRecords;
          this.getPageEntries();
          // this.toastr.success("List")
          this.spinner.hide();
        }
        // console.log("res------>", this.tenantList);
      }, error => {
        console.log("error--->", error);
      });
  }

  // get first and last entry of table
  getPageEntries() {
    const checkFirstNumber = (this.page * this.pageSize) - this.pageSize + 1;
    if (checkFirstNumber > this.totalRecords) {
      this.firstEntry = this.totalRecords;
    } else {
      this.firstEntry = (this.page * this.pageSize) - this.pageSize + 1;
    }
    const checkLastNumber = this.page * this.pageSize;
    if (checkLastNumber > this.totalRecords) {
      this.lastEntry = this.totalRecords;
    } else {
      this.lastEntry = this.page * this.pageSize;
    }
  }

  //page chabge event
  pageChanged(event: any): void {
    this.page = event;
    this.getBatchUsersList();
  }

  // on search data
  onSearchData() {
    this.page = 1;
    this.getBatchUsersList();
  }

  async onPageSize() {
    this.page = 1;
    this.getBatchUsersList();
  }

  async onPhaseAllocation(phasenumber) {
    let journeyDate = '';
    if (phasenumber === 1) {
      journeyDate = this.prePhase;
    } else if (phasenumber === 2) {
      journeyDate = this.inPhase;
    } else if (phasenumber === 3) {
      journeyDate = this.postPhase;
    }
    // console.log("--", this.prePhase)
    // this.inPhase = this.prePhase;
    if (journeyDate !== '') {
      const data = {
        batchname: this.batchData.batchName,
        phasedate: journeyDate,
        phasenumber: phasenumber,
        tenantid: this.tenantData.id
      };
      this.spinner.show();
      await this.tenantService.addBatchPhase(data).subscribe((res: any) => {
        // console.log('res--->', res);
        this.spinner.hide();
        if (res === 200) {
          this.toastr.success('Journey phase allocation added.');
        }
      });
    }
  }

  //View User Navigate
  onView(user) {
    localStorage.setItem('userData', JSON.stringify(user));
    this.router.navigate(['admin/tenant/manage-batches/ticket-list']);
  }

  // Delete User
  onDelete(user) {

  }

  onBack() {
    this.router.navigate(['admin/tenant/manage-batches']);
  }

}

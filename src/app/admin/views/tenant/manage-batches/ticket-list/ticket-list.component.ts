import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TenantService } from './../../../../common/services/tenants/tenant.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {

  tenantData = null;
  batchData = null;
  userData = null;
  ticketList = [];
  pageSize = 10;
  page = 1;
  search = '';
  totalRecords = 0;
  firstEntry = 1;
  lastEntry = 10;
  phase1: any;
  phase2: any;
  phase3: any;
  constructor(
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.tenantData = JSON.parse(localStorage.getItem('tenantData'));
    this.batchData = JSON.parse(localStorage.getItem('batch'));
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.getTicketList();
    this.getJourneyPhase();
  }

  // get Journey Phase
  async getJourneyPhase() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.getJourneyPhase(this.userData.userName).subscribe((res: any) => {
      if (res) {
        console.log("res-1-->", res);
        this.phase1 = res.userJourneyLevelViews[0];
        this.phase2 = res.userJourneyLevelViews[1];
        this.phase3 = res.userJourneyLevelViews[2];
        // this.toastr.success("List")
      }
      this.spinner.hide();
      // console.log("res------>", this.tenantList);
    }, error => {
      console.log("error--->", error);
    });
  }

  onViewResponse() {
    this.router.navigate(['admin/tenant/manage-batches/manage-user']);
  }

  // get Ticket list
  async getTicketList() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.fetchTicketList(1,
      this.page, this.pageSize, this.search).subscribe((res: any) => {
        if (res) {
          this.ticketList = res.results;
          this.totalRecords = res.totalNumberOfRecords;
          this.getPageEntries();
          // this.toastr.success("List")
          this.spinner.hide();
        }
        // console.log("res------>", this.tenantList);
      }, error => {
        console.log("error--->", error);
      });
  }
  // get first and last entry of table
  getPageEntries() {
    const checkFirstNumber = (this.page * this.pageSize) - this.pageSize + 1;
    if (checkFirstNumber > this.totalRecords) {
      this.firstEntry = this.totalRecords;
    } else {
      this.firstEntry = (this.page * this.pageSize) - this.pageSize + 1;
    }
    const checkLastNumber = this.page * this.pageSize;
    if (checkLastNumber > this.totalRecords) {
      this.lastEntry = this.totalRecords;
    } else {
      this.lastEntry = this.page * this.pageSize;
    }
  }

  async onPageSize() {
    this.page = 1;
    this.getTicketList();
  }

  // on search data
  onSearchData() {
    this.page = 1;
    this.getTicketList();
  }

  //page chabge event
  pageChanged(event: any): void {
    this.page = event;
    this.getTicketList();
  }

  onView(ticket) {
    localStorage.setItem('ticketData', JSON.stringify(ticket));
    this.router.navigate(['admin/tenant/manage-batches/view-ticket']);
  }

  onBack(){
    this.router.navigate(['admin/tenant/manage-batches/user-list']);
  }

}

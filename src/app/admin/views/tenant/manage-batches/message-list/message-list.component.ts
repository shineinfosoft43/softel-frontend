import { TenantService } from './../../../../common/services/tenants/tenant.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {

  public tenantData;
  public batchData;
  public tenantId: any;
  pageSize = 10;
  page = 1;
  search = '';
  messageList = [];
  totalRecords = 0;
  firstEntry = 1;
  lastEntry = 10;

  constructor(
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.tenantId = localStorage.getItem('tenantId');
    this.batchData = JSON.parse(localStorage.getItem('batch'));
    this.getTenantDetails();
    this.getMessagesList();
  }

  // get tenant details
  async getTenantDetails() {
    this.spinner.show();
    await this.tenantService.getTenantDetail(this.tenantId).subscribe((res: any) => {
      if (res) {
        this.tenantData = res;
        // console.log("tenantData--->", this.tenantData)
        localStorage.setItem('tenantData', JSON.stringify(res));
      }
      this.spinner.hide();
    });
  }

  // get Message list
  async getMessagesList() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.fetchMessagesList(this.batchData.batchName,
      this.page, this.pageSize, this.search).subscribe((res: any) => {
        if (res) {
          this.messageList = res.results;
          // this.userList = this.demoData;
          this.totalRecords = res.totalNumberOfRecords;
          this.getPageEntries();
          // this.toastr.success("List")
          this.spinner.hide();
        }
        // console.log("res------>", this.tenantList);
      }, error => {
        console.log("error--->", error);
      });
  }

  // get first and last entry of table
  getPageEntries() {
    const checkFirstNumber = (this.page * this.pageSize) - this.pageSize + 1;
    if (checkFirstNumber > this.totalRecords) {
      this.firstEntry = this.totalRecords;
    } else {
      this.firstEntry = (this.page * this.pageSize) - this.pageSize + 1;
    }
    const checkLastNumber = this.page * this.pageSize;
    if (checkLastNumber > this.totalRecords) {
      this.lastEntry = this.totalRecords;
    } else {
      this.lastEntry = this.page * this.pageSize;
    }
  }

  //page chabge event
  pageChanged(event: any): void {
    this.page = event;
    this.getMessagesList();
  }

  // on search data
  onSearchData() {
    this.page = 1;
    this.getMessagesList();
  }

  async onPageSize() {
    this.page = 1;
    this.getMessagesList();
  }

  onBack() {
    this.router.navigate(['admin/tenant/manage-batches']);
  }

}

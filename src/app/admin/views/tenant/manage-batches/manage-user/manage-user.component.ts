import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TenantService } from './../../../../common/services/tenants/tenant.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss']
})
export class ManageUserComponent implements OnInit {

  userData = null;
  questionList = [];
  phase1: any;
  phase2: any;
  phase3: any;


  constructor(
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.getJourneyPhase();
  }

  // get Journey Phase
  async getJourneyPhase() {
    this.spinner.show();
    // this.page ? this.page : 1, this.pageSize ? this.pageSize : 10
    await this.tenantService.getJourneyPhase(this.userData.userName).subscribe((res: any) => {
      if (res) {
        this.questionList = res.questionWithAnswers;
        this.phase1 = res.userJourneyLevelViews[0];
        this.phase2 = res.userJourneyLevelViews[1];
        this.phase3 = res.userJourneyLevelViews[2];
        // this.toastr.success("List")
      }
      this.spinner.hide();
      // console.log("res------>", this.tenantList);
    }, error => {
      console.log("error--->", error);
    });
  }

  onBack(){
    this.router.navigate(['admin/tenant/manage-batches/ticket-list']);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToasterModule, ToasterService } from "angular2-toaster";
import { PerfectScrollbarModule }          from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

export const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
const APP_CONTAINERS = [
  AdminDefaultTemplateComponent
];
//const APP_CONTAINERS = [
  //DefaultAdminComponent
//];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { AdminDefaultTemplateComponent } from './admin-default-template/admin-default-template.component';
import { IconSetService } from '@coreui/icons-angular';



@NgModule({
  declarations: [AdminDefaultTemplateComponent],
  imports: [
    PerfectScrollbarModule,
    CommonModule,
    AdminRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    NgxSpinnerModule,
    ToasterModule.forRoot()
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy 
    },
    ToasterService,
    IconSetService
  ]

})
export class AdminModule { }

/*
    Tenant Api Service
    Created : 12 Feb 2020 by War
*/

// Some needed modules --------- trying removing one
import { Injectable } from "@angular/core";
import { ApiService } from "../api.service";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { environment } from '../../../../../environments/environment';

// Essential Variables and Annotations
@Injectable({
  providedIn: "root",
})

// Export Modules
export class TenantService {
  constructor(private apiService: ApiService) { }

  // fetch tenant list
  fetchTenantList(page, pagesize, searchString): Observable<Response> {
    return new Observable<Response>((req) => {
      let pages = "?page=" + page;
      let pageSize = "&pageSize=" + pagesize;
      let search = "&SearchString=" + searchString;
      if (searchString != "") {
        this.apiService
          .get(`${environment.tenantList}${pages}${pageSize}${search}`)
          .subscribe((res) => {
            req.next(res);
          });
      } else {
        this.apiService
          .get(`${environment.tenantList}${pages}${pageSize}`)
          .subscribe((res) => {
            req.next(res);
          });
      }
    });
  }

  // add new tenant
  addTenant(data): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService
        .post(
          `${environment.addTenant}`,
          data,
          this.apiService._httpOptions_Json
        )
        .subscribe((res) => {
          req.next(res);
        });
    });
  }
  // add new tenant
  updateTenant(data): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService.put(`${environment.addTenant}`, data, this.apiService._httpOptions_Json).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // get tenant details
  getTenantDetail(id): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService.get(`${environment.getTenant}${id}`).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // get Questions List
  getQuestionsList(tenantID, journeyID, phaseID): Observable<Response> {
    let tenantId = '?tenantID=' + tenantID;
    let journeyId = '&journeyPathID=' + journeyID;
    let phaseId = '&Phase=' + phaseID;
    return new Observable<Response>((req) => {
      this.apiService.get(`${environment.getJourneyQuestionList}${tenantId}${journeyId}${phaseId}`).subscribe((res) => {
        req.next(res);
      });
    });
  }


  // get Ticket Priority list data.
  getPriorityList(id): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService.get(`${environment.ticketPriorityData}${id}`).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // get question typ for manage questions.
  getQuestionType() {
    return new Observable<Response>((req) => {
      this.apiService.get(environment.questionType).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // get phase type list
  getPhaseType(id) {
    return new Observable<Response>((req) => {
      this.apiService.get(`${environment.phaseType}${id}`).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // get migration type list for manage questions.
  getMigrationType(id) {
    return new Observable<Response>((req) => {
      this.apiService.get(`${environment.migrationType}${id}`).subscribe((res) => {
        req.next(res);
      });
    });
  }

  // add new tenant
  addQuestion(data): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService
        .post(
          `${environment.JourneyQuestionDefinition}`,
          data)
        .subscribe((res) => {
          req.next(res);
        });
    });
  }

  // fetch tenant user list
  fetchTenantUserList(tenantId, page, pagesize, searchString): Observable<Response> {
    return new Observable<Response>((req) => {
      let tenantID = '?tenantID=' + tenantId;
      let pages = "&page=" + page;
      let pageSize = "&pageSize=" + pagesize;
      let search = "&SearchString=" + searchString;
      if (searchString != "") {
        this.apiService
          .get(`${environment.tenantUserList}${tenantID}${pages}${pageSize}${search}`)
          .subscribe((res) => {
            req.next(res);
          });
      } else {
        this.apiService
          .get(`${environment.tenantUserList}${tenantID}${pages}${pageSize}`)
          .subscribe((res) => {
            req.next(res);
          });
      }
    });
  }

  // add new tenant
  addTenantUserCSV(data): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService
        .post(
          `${environment.tenantUserList}`,
          data,
          this.apiService._httpOptions_Json
        )
        .subscribe((res) => {
          req.next(res);
        });
    });
  }

  // Batch Users List
  fetchBatchUsersList(tenantId, batch, page, pagesize, searchString): Observable<Response> {
    return new Observable<Response>((req) => {
      let tenantID = '?tenantID=' + tenantId;
      let batchName = '&Batchname=' + batch;
      let pages = "&page=" + page;
      let pageSize = "&pageSize=" + pagesize;
      let search = "&SearchString=" + searchString;
      if (searchString != "") {
        this.apiService
          .get(`${environment.batchUsers}${tenantID}${batchName}${pages}${pageSize}${search}`)
          .subscribe((res) => {
            req.next(res);
          });
      } else {
        this.apiService
          .get(`${environment.batchUsers}${tenantID}${batchName}${pages}${pageSize}`)
          .subscribe((res) => {
            req.next(res);
          });
      }
    });
  }

  // Messages List
  fetchMessagesList(user, page, pagesize, searchString): Observable<Response> {
    return new Observable<Response>((req) => {
      let endpoint = 'AdminPersonalMessage';
      let userName = '?userName=' + user;
      let pages = "&page=" + page;
      let pageSize = "&pageSize=" + pagesize;
      let search = "&SearchString=" + searchString;
      if (searchString != "") {
        this.apiService
          .get(`${endpoint}${userName}${pages}${pageSize}${search}`)
          .subscribe((res) => {
            req.next(res);
          });
      } else {
        this.apiService
          .get(`${endpoint}${userName}${pages}${pageSize}`)
          .subscribe((res) => {
            req.next(res);
          });
      }
    });
  }

  // Get Phase Allocation Date
  getPhaseAllocationDate(tenantId, batch): Observable<Response> {
    return new Observable<Response>((req) => {
      let endpoint = 'BatchPhase';
      let tenantID = '?tenantID=' + tenantId;
      let batchName = '&Batchname=' + batch;
      this.apiService
        .get(`${endpoint}${tenantID}${batchName}`)
        .subscribe((res) => {
          req.next(res);
        });
    });
  }

  // Get Journey Phase
  getJourneyPhase(users): Observable<Response> {
    return new Observable<Response>((req) => {
      let endpoint = 'BatchUserStatus/';
      let user = users;
      this.apiService
        .get(`${endpoint}${user}`)
        .subscribe((res) => {
          req.next(res);
        });
    });
  }

  // add Batch Phase date
  addBatchPhase(data): Observable<Response> {
    return new Observable<Response>((req) => {
      this.apiService
        .put(
          `${environment.batchPhase}`,
          data
        )
        .subscribe((res) => {
          req.next(res);
        });
    });
  }

  // get Ticket List
  fetchTicketList(userId, page, pagesize, searchString): Observable<Response> {
    return new Observable<Response>((req) => {
      let userID = '?UserID=' + userId;
      let pages = "&page=" + page;
      let pageSize = "&pageSize=" + pagesize;
      let search = "&SearchString=" + searchString;
      if (searchString != "") {
        this.apiService
          .get(`${environment.userTickets}${userID}${pages}${pageSize}${search}`)
          .subscribe((res) => {
            req.next(res);
          });
      } else {
        this.apiService
          .get(`${environment.userTickets}${userID}${pages}${pageSize}`)
          .subscribe((res) => {
            req.next(res);
          });
      }
    });
  }

}

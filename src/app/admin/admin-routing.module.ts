import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDefaultTemplateComponent } from './admin-default-template/admin-default-template.component';
import { HomeComponent } from './home/home.component';
import { DashboardModule } from './views/dashboard/dashboard.module';
import { TenantdModule } from './views/tenant/tenant.module';
 


const routes: Routes = [
  {
    path: '',
    redirectTo: 'tenant',
    pathMatch: 'full',
  },
  //{
  //  path: 'login',
  //  component: LoginComponent,
  //  data: {
  //    title: 'Login'
  //  }
  //},
  {
    path: '',
    component: AdminDefaultTemplateComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        //component: DashboardModule,
       loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'tenant',
        //component: TenantdModule,
        loadChildren: () => import('./views/tenant/tenant.module').then(m => m.TenantdModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

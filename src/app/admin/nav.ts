import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/admin/dashboard',
    icon: 'fa fa-home'
    // badge: {
    //   variant: 'info',
    //   text: 'NEW'
    // }
  },
  {
    name: 'Tenant',
    url: '/admin/tenant',
    icon: 'fa fa-user'
  }
  // {
  //   title: true,
  //   name: 'Title'
  // },
  // {
  //   name: 'Disabled',
  //   url: '/dashboard',
  //   icon: 'icon-ban',
  //   attributes: { disabled: true },
  // }
];



import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDefaultTemplateComponent } from './admin-default-template.component';

describe('AdminDefaultTemplateComponent', () => {
  let component: AdminDefaultTemplateComponent;
  let fixture: ComponentFixture<AdminDefaultTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDefaultTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDefaultTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

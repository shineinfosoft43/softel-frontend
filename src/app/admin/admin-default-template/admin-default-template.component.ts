import { Component, OnInit } from '@angular/core';
import { navItems } from '../nav';

@Component({
  selector: 'app-admin-default-template',
  templateUrl: './admin-default-template.component.html',
  styleUrls: ['./admin-default-template.component.scss']
})
export class AdminDefaultTemplateComponent implements OnInit {
  minimized = false;
  public navItems = navItems;
  constructor() { }

  ngOnInit(): void {
  }
  
  toggleMinimize(e) {
    this.minimized = e;
  }

}

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttperrordisplayService } from './service/httperrordisplay.service';
 
 
@Injectable()
export class AppHttpErrorInterceptor implements HttpInterceptor {
  constructor(private errorDisplay: HttperrordisplayService) {
  }
  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    return next.handle(httpRequest)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {
          // debugger;
          this.errorDisplay.handleError(error);
     
          //this.responseMessage = msg;
          // this.myModal.show();
          return throwError(error);
        })
      )

  }



}


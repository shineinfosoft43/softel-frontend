import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authentication-error',
  templateUrl: './authentication-error.component.html',
  styleUrls: ['./authentication-error.component.scss']
})
export class AuthenticationErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from "@angular/common/http";
import { ErrorService } from './ErrorService';
 
 


@Injectable({
  providedIn: 'root'
})
export class HttperrordisplayService {

  constructor(private toastrs: ToastrService, private errorapi: ErrorService) {
   
  }

  public showError(message:string) {
    this.toastrs.error(message);
  }

  public showInformation(message: string) {
    this.toastrs.show(message);
  }

  public showSuccess(message: string) {
    this.toastrs.success(message);
  }

  public showWarming(message: string) {
    this.toastrs.warning(message);
  }

  // Handling HTTP Errors using Toaster
  public handleError(error: HttpErrorResponse) {
    
  //  this.toastrs.overlayContainer = this.toastContainer;
    let errorMessage = '';
 
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    var msg = "";
    if (error.status == 500) {
      msg = "Intenal server error"
    }
    else if (error.status == 404) {
      msg = "Data Error"
    }
    else {
      msg = "We are sorry but the site is experiencing technical difficulties"
    }
    //debugger;
    this.errorapi.changeError(msg);
    this.toastrs.error(msg);

  }

}

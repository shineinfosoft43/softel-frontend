import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { CustomAuthGuardService } from './custom-auth-guard.service';
import { UserDetailsService } from './user-details.service';

describe('CustomAuthGuardService', () => {
  let service: CustomAuthGuardService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [UserDetailsService]
    });
    service = TestBed.inject(CustomAuthGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PersonalMessageService } from './personal-message.service';

describe('PersonalMessageService', () => {
  let service: PersonalMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule,RouterTestingModule],
    });
    service = TestBed.inject(PersonalMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class AuthTokenService {

  private tokenSource = new BehaviorSubject('');
  
  currentToken = this.tokenSource.asObservable();
  
  constructor() { }

  changeMessage(message: string) {
    this.tokenSource.next(message)
  }

   



}

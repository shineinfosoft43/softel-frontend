import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
   } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
   import { Observable, throwError } from 'rxjs';
   import { retry, catchError } from 'rxjs/operators';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
   
   export class HttpErrorInterceptor implements HttpInterceptor {
    //constructor(private modalService: BsModalService){}
    // @ViewChild('myModal') public myModal: ModalDirective;
    // responseMessage: string;
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request)
        .pipe(
          retry(1),
          catchError((error: HttpErrorResponse) => {
            ////debugger;
            let errorMessage = '';
            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = `Error: ${error.error.message}`;
            } else {
              // server-side error
              errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
            var msg="";
            if(error.status==500){
              msg="Intenal server error"
            }
            else if(error.status==404){
              msg="Data Error"
            }        
            else{
              msg ="We are sorry but the site is experiencing technical difficulties"
            }
            const initialState = {
              errormsg: msg
            };
            let  bsModalRef: BsModalRef
           // bsModalRef = this.modalService.show(ErrorModalComponent, { initialState });
            //errorMessage=errorMessage.replace(error.url,"");
             window.alert(msg);
            //this.responseMessage = msg;
           // this.myModal.show();
            return throwError(errorMessage);
          })
        )
    }
   }

import { TestBed } from '@angular/core/testing';

import { JwtCheckService } from './jwt-check.service';

describe('JwtCheckService', () => {
  let service: JwtCheckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JwtCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

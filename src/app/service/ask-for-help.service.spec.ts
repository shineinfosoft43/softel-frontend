import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';


import { AskForHelpService } from './ask-for-help.service';

describe('AskForHelpService', () => {
  let service: AskForHelpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
      
    });
    service = TestBed.inject(AskForHelpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

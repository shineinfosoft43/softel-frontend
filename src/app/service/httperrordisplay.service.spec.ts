import { TestBed } from '@angular/core/testing';

import { HttperrordisplayService } from './httperrordisplay.service';

describe('HttperrordisplayService', () => {
  let service: HttperrordisplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttperrordisplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

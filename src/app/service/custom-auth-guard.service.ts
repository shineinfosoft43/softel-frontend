import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { environment } from '../../environments/environment';
import { JwtCheckService } from './jwt-check.service';
import { UserDetailsService } from './user-details.service';
import { StoreTokenService } from './store-token-service.service';
import { UserLoginService } from './user-login.service';
@Injectable({
  providedIn: 'root'
})
export class CustomAuthGuardService implements CanActivate {

  constructor(private jwtService: JwtCheckService,
    private authStorageService: StoreTokenService,
    private router: Router,
    private userService: UserDetailsService,
    private auth: UserLoginService) { }
  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    //debugger;
    this.jwtService.setToken(this.authStorageService.get("auth"));
    let cnt = 0;
   // console.log("before while");
    while (this.jwtService.getUser()) {
      if (this.jwtService.isTokenExpired()) {
        this.auth.Login();
        this.jwtService.setToken(this.authStorageService.get("auth"));
      }
      else {
        //console.log("break");
        break;
      }
      cnt = cnt + 1;
      if (cnt == 2) {
        //console.log("error ");
        this.router.navigate(['/autherror']);
        return false;
      }
    }
    if (!this.jwtService.getUser()) {
      this.router.navigate(['/autherror']);
      return false;
    }
    const email = this.jwtService.getEmailId();
    const res = await this.userService.validateUser(email);
    //debugger
    if (res.isMigrationUser) {
      return true;
    } else {
      this.router.navigate(['/autherror']);
      return false;
    }
  }
}

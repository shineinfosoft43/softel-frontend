// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  GraphURL: "https://graph.microsoft.com/v1.0/me",
  APIURL: "https://softeltestapi.azurewebsites.net/",
  ClientID: "b4ee3df9-abff-4c0b-9d4d-408295a95707",
  RedirectURL: "https://journeyautomation.softel.com",
  TenantID: "6215ebd7-b37d-4bc7-a032-1a2ea90dc0b9",
  APIResourceScope: "api://b4ee3df9-abff-4c0b-9d4d-408295a95707/access_as_user",
  Authority: "https://login.microsoftonline.com/",
  tenantList: 'tenantsummary',
  tenantUserList: 'BatchUpload',
  addTenant: 'Tenant',
  getTenant: 'Tenant/',
  phaseType: 'TenantPhase/',
  getJourneyQuestionList: 'JourneyQuestionDefinition',
  migrationType: 'JourneyPath/',
  questionType: 'QuestionType',
  ticketPriorityData: 'QuestionPriority/',
  JourneyQuestionDefinition: 'JourneyQuestionDefinition',
};


